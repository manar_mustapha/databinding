package com.example.manar.bindview;

/**
 * Created by manar on 13/09/17.
 */

public class User {

    public User(String name, String image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    String name ;
    String image;
}
