package com.example.manar.bindview;

import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;


import android.view.View;
import android.view.ViewGroup;



import java.util.ArrayList;
import java.util.List;

/**
 * Created by manar on 13/09/17.
 */

public class RecycleViewAdapter extends RecyclerView.Adapter<RecycleViewAdapter.ViewHolder> {


    List<User> user;

    public RecycleViewAdapter() {
        user = new ArrayList<>();
        user.add(new User("manar", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("hello", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("yehoo", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("aaaaaa", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("manvvvvvar", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("vvvvv", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("ccccc", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("sssss", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("gggg", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));
        user.add(new User("mmmmm", "https://www.google.com.eg/search?q=images&client=ubuntu&hs=K75&channel=fs&dcr=0&tbm=isch&tbo=u&source=univ&sa=X&ved=0ahUKEwjv9fu65aHWAhXCK1AKHTW8BuAQsAQIMQ&biw=1680&bih=955#imgrc=TCzIUatmlfq74M:"));

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        RecycleViewItemBinding itemBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.recycle_view_item, parent, false);
        return new ViewHolder(itemBinding);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.binding.setProduct(user.get(position));
    }

    @Override
    public int getItemCount() {
        return user.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        RecycleViewItemBinding binding;

        public ViewHolder(RecycleViewItemBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }


    }


}
